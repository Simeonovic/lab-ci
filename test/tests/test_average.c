#include "average.h"
#include "unity.h"

#include <stdlib.h>
#include <string.h>

void test_avg_function(void)
{
    /* Check result */
    TEST_ASSERT_EQUAL_INT_MESSAGE(1, integer_avg(1,1), "Error in integer_avg pos");
    TEST_ASSERT_EQUAL_INT_MESSAGE(5, integer_avg(5,5), "Error in integer_avg pos");
    TEST_ASSERT_EQUAL_INT_MESSAGE(7, integer_avg(10,5), "Error in integer_avg cast int positif");
    TEST_ASSERT_EQUAL_INT_MESSAGE(8, integer_avg(10,6), "Error in integer_avg pos");
    TEST_ASSERT_EQUAL_INT_MESSAGE(-5, integer_avg(-4,-6), "Error in integer_avg neg");
    TEST_ASSERT_EQUAL_INT_MESSAGE(-5, integer_avg(-5,-6), "Error in integer_avg cast int negatif");
}

